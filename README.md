# A REST/websocket server providing a few Uniswap-related endpoints.

## Usage
Run `npm install` to download all dependencies before running the server.
```
❯ npm start -- --help

> minimal-uniswap-api@1.0.0 start
> node ./src/index.js "--help"

Usage: index npm start -- [options]

Minimal REST/websocker server providing a few Uniswap-V2 related endpoints.

Options:
  -p, --pk <string>  private key to enabling trading on uniswap.
  --dev              if set, CHAIN_ID is set to that of Goerli testnet.
  -h, --help         display help for command
```

```
❯ npm start

> minimal-uniswap-api@1.0.0 start
> node ./src/index.js

server listening on port 3000
```

## Endpoints:

1. **GET /price/:factory/:asset1/:asset2**
Return the current price (__midPrice__) for the pool defined by the factory defined by asset1/asset2.

2. **WEBSOCKET /feed/:factory/:asset1/:asset2**
Subscribes to a websocket connection which emits new prices whenever the **asset1/asset2 UNI-V2 LP contract** emits a **Swap** event.

One thing to note is that the currently implementation supports maximum of one websocket server, meaning in order to get a price feed for a different pair, you need to close all existing connections first. One can easily modify the implementation to track and store a list of WS servers by their asset pair addresses.

3. **POST /trade**
Places a trade on Uniswap V2 if a JSON input with the following schema is sent to server:
```
{
	path: string[]
	amountIn: string
	slippage: number
} 

Factory and Router02 addresses, since not provided, are hardcoded and works for all ETH mainnnet/testnets.

## Notes
Sometimes same price feed will be emitted multiple times because within one block the Swap event was emitted multiple times accordingly.
