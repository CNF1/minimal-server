const { ethers } = require('ethers');

const ETH_MAINNET_PERSONAL_RPC = 'https://eth-mainnet.alchemyapi.io/v2/953oofWAlCodq-5bkD0ZyZ7UqHsa9zNl'
const GOERLI_PERSONAL_RPC = 'https://eth-goerli.alchemyapi.io/v2/9NHSj5Ve9eWUoy8z9QmR5BvTgYxvhCg5'

function setupConnection(pk) {
    const provider = (global.CHAIN_ID == 1 ? new ethers.providers.JsonRpcProvider(ETH_MAINNET_PERSONAL_RPC)
                                           : new ethers.providers.JsonRpcProvider(GOERLI_PERSONAL_RPC))
    
    if (!pk) {
        return provider
    } else {
        try {
            const wallet = new ethers.Wallet(pk).connect(provider)
            return wallet
        } catch (e) {
            console.error(`[ERROR] Cannot initiate ethers wallet with the provided private key: ${e}`)
            process.exit(1);
        }
    }
}

module.exports = { setupConnection }
