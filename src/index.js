const { program } = require('commander')
const { WebSocket, WebSocketServer } = require("ws")
const express = require('express')
const http = require('http')

const { setupConnection } = require('./web3.js');
const { getMidPrice, getPairContract, getMidPriceFromAddress, tradeExactPathExactIn } = require('./utils.js')

const port = 3000
program
    .description("Minimal REST/websocker server providing a few Uniswap-V2 related endpoints.")
    .usage('npm start -- [options]')
    .option("-p, --pk <string>", "private key to enabling trading on uniswap.")
    .option("--dev", "if set, CHAIN_ID is set to that of Goerli testnet.")
    .parse(process.argv);

const options = program.opts();

global.CHAIN_ID = options.dev ? 5 : 1;
if (options.pk) {
    global.wallet = setupConnection(options.pk);
} else {
    global.provider = setupConnection();
}

const app = express();

app.use(express.json())
app.get('/', (_, res) => {
    res.send("hello uniswap")
});

app.get('/price/:factory/:asset1/:asset2', getMidPrice);

app.post('/trade', tradeExactPathExactIn);

const server = app.listen(port, () => {
    console.log(`server listening on port ${port}`)
});

const wss = new WebSocketServer({ server: server });

wss.on('connection', async function (ws, req) {
    params = req?.url?.split('/');

    if (params[1] !== 'feed' || params[2] === undefined || params[3] === undefined || params[4] === undefined) {
        console.error('[ERROR] Price feeds are serverd on /feed/:factory/:asset1/:asset2 endpoint. Closing connection');
        ws.close();
    }
    try {
        const pairContract = await getPairContract(params[2], params[3], params[4]);
        pairContract.on("Swap", async () => { 
            let newPrice = await getMidPriceFromAddress(params[2], params[3], params[4]);
            ws.send(newPrice);
        });

        ws.on('close', () => {
            pairContract.removeAllListeners()
        })
    } catch (e) {
        console.error('[ERROR] Unable to monitor pair trades and/or send price feeds.')
        console.error(e)
        ws.close();
    }
});