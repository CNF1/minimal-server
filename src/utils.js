const ethers = require('ethers')

const { computePairAddress, Pair, Route, Trade } = require('@uniswap/v2-sdk')
const { CurrencyAmount, Percent, Token, TradeType, WETH9 } = require('@uniswap/sdk-core')
const ERC20 = require('./ABI/ERC20.json')
const IUniswapV2Pair = require('@uniswap/v2-core/build/IUniswapV2Pair.json')
const UniswapV2Router02 = require('./ABI/UniswapV2Router02.json')
const JSBI = require('jsbi')
const { Contract } = require('ethers')
const ws = require('ws')

const FACTORY_MAINNET = '0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f'
const ROUTER = '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D'
var TOKENS_CACHE = new Map()

function getCurrentProvider() {
    if (global.wallet === undefined) {
        return global.provider
    } else {
        return global.wallet.provider
    }
}

async function getPairAddressFromAddress(factory, asset1, asset2) {
    const tokenA = await getToken(asset1, getCurrentProvider())
    const tokenB = await getToken(asset2, getCurrentProvider()) 
    const address = computePairAddress({ factoryAddress: factory, tokenA, tokenB })
    return address
}

async function getToken(address) {
    if (!TOKENS_CACHE.has(address)) {
        let tokenContract = new Contract(address, ERC20, getCurrentProvider())
        let decimal = await tokenContract.decimals()
        TOKENS_CACHE.set(address, new Token(global.CHAIN_ID, address, decimal))
    }
    return TOKENS_CACHE.get(address)

}

async function getPair(factoryAddr, tokenA, tokenB) {
    const address = computePairAddress({ factoryAddress: factoryAddr, tokenA, tokenB })
    const [reserves0, reserves1] = await new Contract(address, IUniswapV2Pair.abi, getCurrentProvider()).getReserves()
    const balances = tokenA.sortsBefore(tokenB) ? [reserves0, reserves1] : [reserves1, reserves0]
    return new Pair(CurrencyAmount.fromRawAmount(tokenA, balances[0]), CurrencyAmount.fromRawAmount(tokenB, balances[1]))
}

async function getPairMainnet(tokenA, tokenB) {
    // works for --dev (goerli) as well, since uni v2 factory is deployed to the same address
    return await getPair(FACTORY_MAINNET, tokenA, tokenB)
}

async function getPairContract(factoryAddr, asset1Addr, asset2Addr) {
    const address = await getPairAddressFromAddress(factoryAddr, asset1Addr, asset2Addr)
    var abi = [
        'event Swap(address indexed sender, uint amount0In, uint amount1In, uint amount0Out, uint amount1Out,address indexed to)'
    ]
    const contract = new ethers.Contract(address, abi, getCurrentProvider())
    return contract
}

async function getMidPriceFromAddress(_factory, _asset1, _asset2) {
    const tokenA = await getToken(_asset1)
    const tokenB = await getToken(_asset2)
    const pair = await getPair(_factory, tokenA, tokenB)
    const route = new Route([pair], tokenA, tokenB)
    return route.midPrice.toSignificant(tokenB.decimals)
}

async function getMidPrice(req, res) {
    let price = await getMidPriceFromAddress(req.params.factory, req.params.asset1, req.params.asset2)
    res.send(price)
}

async function tradeExactPathExactIn(req, res) {
    if (!global.wallet) {
        console.error('[ERROR] Server was not initialized with a valid private key. Ignoring trade request.')
        return
    } 
    const slippagePercent = new Percent(req.body.slippage, '100')
    try {
        const pathInAddress = req.body.path
        const pathInPairs = []
        const sourceToken = await getToken(pathInAddress[0])
        const destToken = await getToken(pathInAddress[pathInAddress.length - 1])
        for (let i = 1; i < pathInAddress.length; i++) {
            let fromToken = await getToken(pathInAddress[i-1])
            let toToken = await getToken(pathInAddress[i])
            let pair = await getPairMainnet(fromToken, toToken)
            pathInPairs.push(pair)
        }
        const tradeRoute = new Route(pathInPairs, sourceToken, destToken)
        const trade = new Trade(tradeRoute, CurrencyAmount.fromRawAmount(sourceToken, req.body.amountIn), TradeType.EXACT_INPUT)
        const minAmountOut = trade.minimumAmountOut(slippagePercent)
        const amountIn = ethers.BigNumber.from(req.body.amountIn)

        const router = new ethers.Contract(ROUTER, UniswapV2Router02, global.wallet)

        const deadline = Math.floor(Date.now() / 1000) + 60 * 5; // 5 minutes from current unix time
        const minAmountOutBigNumber = ethers.BigNumber.from(minAmountOut.quotient.toString())
        let tx;
        if (sourceToken.equals(WETH9[1]) || sourceToken.equals(WETH9[5])) {
            tx = await router.swapExactETHForTokens(
                minAmountOutBigNumber,
                pathInAddress,
                global.wallet.address,
                deadline,
                {
                    value: amountIn
                }
            )
        } else if (destToken.equals(WETH9[1]) || destToken.equals(WETH9[5])) {
            tx = await router.swapExactTokensForETH(amountIn, minAmountOutBigNumber, pathInAddress, global.wallet.address, deadline)
        } else {
            tx = await router.swapExactTokensForTokens(amountIn, minAmountOutBigNumber, pathInAddress, global.wallet.address, deadline)
        }
        let responseMsg = `Oplaced trade! TX: ${tx.hash} on chain id ${global.CHAIN_ID}`
        console.log(responseMsg)
        res.send(responseMsg)
    } catch (e) {
        console.error(`[ERROR] Unable to submit a valid trade`)
        console.error(e)
        res.status(500).send()
    }

}

module.exports = { getCurrentProvider, getMidPrice, getMidPriceFromAddress, getPairContract, tradeExactPathExactIn }